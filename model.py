from tools import *
import random
import sys

class Layer():
    def __init__(self, weights, Activation):
        self.input = []
        self.weights = weights
        self.Activation = Activation
        self.output = []
        self.adder = [] #выход сумматоров без применения функции активации
        self.errors = []

    def CalcOutput(self, input):
        self.input = input
        self.output = []
        self.adder = []
        for weight_ind in range(len(self.weights[0])):
            sum = 0
            for ind in range(len(self.weights)):
                sum += float(self.input[ind]) * self.weights[ind][weight_ind]
            self.adder.append(sum + 1)
        self.output = self.Activation(self.adder)

        return self.output


    # На выходном уровне считается кросс энтропия,
    # т.к. для множественного классификатора для выходного слоя нужна функция активации SoftMax
    # и поэтому при подсчете ошибок выходного слоя нужно использовать именно эту формулу
    def CalcErrorsOutputLayer(self, expected_tensors):
        self.errors = []

        print('np loss: ', - np.sum(np.multiply(expected_tensors, np.log(self.output + np.finfo(float).eps)) + np.multiply((1 - np.array(expected_tensors)), np.log(1 - np.array(self.output) + np.finfo(float).eps))))
        self.errors = - np.multiply(expected_tensors, np.log(self.output + np.finfo(float).eps)) + np.multiply((1 - np.array(expected_tensors)), np.log(1 - np.array(self.output) + np.finfo(float).eps))
        return self.errors


    # Подсчет ошибки для скрытых слоев, где функция активации сигмойда
    def CalcErrorsHiddenLayer(self, errors_forward_layer):
        self.errors = []
        for ind in range(len(self.input)):
            sum = 0
            df = self.input[ind] * (1 - self.input[ind])
            for error_ind in range(len(errors_forward_layer)):
                sum += errors_forward_layer[error_ind] * self.weights[ind][error_ind]
            self.errors.append(sum * df)


        return self.errors

    def ToCorrectWeights(self, errors_forward_layer, training_rate):
        for ind in range(len(self.input)):
            for weight_ind in range(len(self.weights[ind])):
                self.weights[ind][weight_ind] += training_rate * errors_forward_layer[weight_ind] * self.input[ind]
        return 0

class Network():
    def __init__(self, layers=[], input_tensors=[], expected_tensors=[], training_rate=0, epochs=0):
        self.layers = layers
        self.input_tensors = input_tensors # обучающий (тестовый) сет
        self.expected_tensors = expected_tensors # идеальные ответы
        self.prediction_tensor = []
        self.training_rate = training_rate # скорость обучения
        self.epochs = epochs


    # сохранение весов. для каждого слоя весов отдельный файл (так удобнее)
    def SaveWeights(self):
        for ind in range(len(self.layers)):
            with open("dataset/weights/weights_" + str(ind) + "_layer.csv", "w", newline="") as file:
                writer = csv.writer(file)
                writer.writerows(self.layers[ind].weights)
        return 0



    def InitializeWeights(self):
        for ind in range(len(self.layers)):
            fname = "dataset/weights/weights_" + str(ind) + "_layer.csv"
            if os.path.exists(fname):
                with open(fname, "r") as file:
                    weights = csv.reader(file, delimiter=',')
                    self.layers[ind].weights = []
                    for row_weights in weights:
                        self.layers[ind].weights.append([float(val) for val in row_weights])
            else:
                print('file does not exists')
                return False
        return True



    def Forward(self, input):
        for layer in self.layers:
            input = layer.CalcOutput(input)

        # подсчет выходного слоя идет с применением sofmax
        self.prediction_tensor = SoftMax(input)
        return self.prediction_tensor


    def Backward(self, expected):
        count_layers = len(self.layers)

        # сначала считаем ошибку для выходного слоя, т.к. для нее применяется
        # отличная от остальных слоев формула по подсчету ошибки
        errors = self.layers[count_layers - 1].CalcErrorsOutputLayer(expected)

        for ind_layer in reversed(range(len(self.layers))):
            errors_forward_layer = errors
            errors = self.layers[ind_layer].CalcErrorsHiddenLayer(errors_forward_layer)
            # после подсчета ошибки в слое - корректируем веса
            self.layers[ind_layer].ToCorrectWeights(errors_forward_layer, self.training_rate)

        return 0

    def Train(self):
        for epoch in range(self.epochs):
            for ind in range(len(self.expected_tensors)):
                output_tensor = self.Forward(self.input_tensors[ind])
                print('\n\n\n\n')


                print('output_tensor: ', output_tensor)
                print('expected: ', self.expected_tensors[ind])
                print('epoch: ', epoch, '; iteration: ', ind)
                self.Backward(self.expected_tensors[ind])
        self.SaveWeights()
        return 0
